import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-switch-toggle',
  templateUrl: './switch-toggle.component.html',
  styleUrls: ['./switch-toggle.component.scss']
})
export class SwitchToggleComponent {
  @Input() checkedToggleSwitch: boolean = false;
  @Input() disabled: boolean = false;
  @Input() item: any;

  @Output() checkToggleSwitch = new EventEmitter<boolean>();

  onClickedToggleSwitch(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    this.checkedToggleSwitch = inputElement.checked;
  
    this.checkToggleSwitch.emit(this.checkedToggleSwitch);
  }
}
