import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrl: './radio-button.component.scss'
})
export class RadioButtonComponent {
  @Input() checkedRadio: boolean = false;
  @Input() disabled: boolean = false;

  @Output() checkedRadioChange = new EventEmitter<boolean>();

  onCheckboxChange(event: any) {
    this.checkedRadio = event.target.checked;
    this.checkedRadioChange.emit(this.checkedRadio);
  }

}
