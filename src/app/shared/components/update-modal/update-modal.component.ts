import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-update-modal',
  templateUrl: './update-modal.component.html',
  styleUrls: ['./update-modal.component.scss']
})
export class UpdateModalComponent {
  @Input() getFormConfig: any;
  @Input() item: any;
  @Input() roleGetListingData: any[] = [];
  @Input() tableConfigGetUpdateData: any;
  @Input() headingTitle: any;

  @Output() updatedData = new EventEmitter<void>();

  selectedRole: any;
  form: FormGroup;



  constructor(private fb: FormBuilder, public activeModal: NgbActiveModal, private toaster: ToastrService) {
    this.form = this.fb.group({});
  }

  ngOnInit() {
    this.createForm();
    
  }

  createForm() {
    this.getFormConfig.forEach((field: { value: any; validations: any; name: any; type?: string; role: { id: number } }) => {
      let control;
      if (field.type === 'dropdown') {
        const objId = this.item[field.name] || null;
        control = new FormControl(objId, this.bindValidations(field.validations || []));
      } else if (field.type === 'array') {
        const obj = this.item[field.name] || [];
        const arrayControls: FormControl[] = obj.map((el: any) => new FormControl(el, this.bindValidations(field.validations || [])));
        control = new FormArray(arrayControls);
      } else {
        control = new FormControl(this.item[field.name] || '', this.bindValidations(field.validations || []));
      }
      this.form.addControl(field.name, control);
     
    });
  }


  addInput(inputValue: string, name: string) {
    if (inputValue) {
      const obj = this.form.get(name) as FormArray
      obj.push(new FormControl(inputValue, this.bindValidations([])));
      
    }
  }
  removeInput(i: number, name: string) {
    const obj = this.form.get(name) as FormArray
    if (obj.length > 1) {
      obj.removeAt(i)
    } else {
      obj.reset()
    }
  }


  bindValidations(validations: any[]) {
    if (validations.length > 0) {
      const validList: any[] = [];
      validations.forEach((valid: { validator: any; }) => {
        validList.push(valid.validator);
      });
      return Validators.compose(validList);
    }
    return null;
  }

 

  
  onSubmit() {
    console.log('on submit called')
    if (this.form.valid) {
      this.tableConfigGetUpdateData.service[this.tableConfigGetUpdateData.apiFunctions.update](this.form.value).subscribe(
        (res: any) => {
          if (res.success) {
            const updatedData = res.data;
            this.toaster.success('Data updated successfully!');
            this.updatedData.emit(updatedData); // Emit event after successful update
            this.activeModal.close();
          }
          this.activeModal.close();
        },
        (error: any) => {
          console.log(error, 'update user error');
          if (error.error && error.error.message) {
            alert(error.error.message.join(', '));  // Display server-side validation errors
          }
        }
      );
    } else {
      this.validateAllFormFields(this.form);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  onRoleSelect(event: any) {
    this.selectedRole = event;
  }
}
