import { Component } from '@angular/core';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss',
  imports: [RouterModule],
  standalone: true
})
export class HeaderComponent {
  constructor(private router: Router){}
  // register(){
  //   this.router.navigate(['/register'])
  // }
}
