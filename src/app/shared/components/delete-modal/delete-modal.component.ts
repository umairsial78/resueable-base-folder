import { Component, Input, } from '@angular/core';
import { NgbActiveModal, NgbModal,  } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrl: './delete-modal.component.scss'
})
export class DeleteModalComponent {
  @Input() item: any;
  @Input() tableConfigGetServiceData: any;
 

  constructor(public activeModal: NgbActiveModal, private modalService: NgbModal  ) {}



  deleteItem() {
    console.log(this.tableConfigGetServiceData, 'check data')
    this.tableConfigGetServiceData.service[this.tableConfigGetServiceData.apiFunctions.delete]({id:null}).subscribe((res:any) => {
      console.log(res);
    })
    // Implement delete logic here
    // You can emit an event to inform the parent component about the deletion
    // For now, just close the modal
    this.activeModal.close();
  }



  // deleteItem() {
  //   console.log(this.tableConfig, 'check data');
  //   if (this.tableConfig && this.tableConfig.serviceData) {
  //     const serviceData = this.tableConfig.serviceData;
  //     const deleteApiFunction = serviceData.service[serviceData.apiFunctions.delete];

  //     if (deleteApiFunction) {
  //       deleteApiFunction({ id: this.item.id }, `${serviceData.url}/delete`).subscribe((res: any) => {
  //         console.log(res);
  //         this.activeModal.close('deleted');
  //       }, (err: any) => {
  //         console.error('Error deleting item:', err);
  //       });
  //     } else {
  //       console.error('Delete function not found in serviceData.apiFunctions');
  //     }
  //   } else {
  //     console.error('Service data or tableConfig is undefined');
  //   }
  // }

}
