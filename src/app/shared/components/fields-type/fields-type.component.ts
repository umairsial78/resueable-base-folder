import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-fields-type',
  templateUrl: './fields-type.component.html',
  styleUrl: './fields-type.component.scss'
})
export class FieldsTypeComponent {
  @Input() field: any;
  @Input() data: any;

  @Input() tableConfig: any;

  @Output() fieldChangeCheck = new EventEmitter<any>();
  @Output() fieldChangeRadio = new EventEmitter<any>();
  @Output() fieldChangeToogleSwitch = new EventEmitter<any>();

  // dataa = [56756757657, 789789798, '021120021', '87989990809'];
  // fields = { type: 'array', isEditable: false };

  onFieldChangeCheck() {
    this.fieldChangeRadio.emit(this.field);
  }

  onFieldChangeRadio() {
    this.fieldChangeRadio.emit(this.field);
  }

  onFieldChangeToggleSwitch() {
    this.fieldChangeToogleSwitch.emit(this.field);
  }

  // isNumber(data: any): boolean {
  //   return typeof data === 'number';
  // }
  
  

}
