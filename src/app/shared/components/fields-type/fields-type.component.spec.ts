import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FieldsTypeComponent } from './fields-type.component';

describe('FieldsTypeComponent', () => {
  let component: FieldsTypeComponent;
  let fixture: ComponentFixture<FieldsTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FieldsTypeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FieldsTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
