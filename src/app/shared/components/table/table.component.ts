import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DeleteModalComponent } from '../delete-modal/delete-modal.component';
import { AdministrationService } from '../../services/administration.service';
import { UpdateModalComponent } from '../update-modal/update-modal.component';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrl: './table.component.scss'
})
export class TableComponent {

  constructor(private modalService: NgbModal, private getUserList: AdministrationService) { }

  @Input() tableConfig: any;
  // @Input() roleListingData: any[] = [];

  @Output() editClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() deleteClicked: EventEmitter<any> = new EventEmitter<any>();
  @Output() search: EventEmitter<string> = new EventEmitter<string>();

  searchQuery: string = '';
  sortColumn: string = '';
  sortDirection: string = '';

 




  ngOnInit() {
    
    // console.log(this.tableConfig.formConfig, 'data tableconfig')
  }


  onSearch() {
    this.search.emit(this.searchQuery);
  }

  onEditClick(item: any) {
    this.editClicked.emit(item);
  }


  onViewClick(){
    this.tableConfig.serviceData.service[this.tableConfig.serviceData.apiFunctions.refresh](this.tableConfig.serviceData.url+'/listing').subscribe((result: any)=>{
      if(result){
        console.log(result, 'user data refresh')
      }
    })
    // this.getUserList.getUserList({}).subscribe((data)=>{
    //   console.log(data, 'userlisting')

    // })

  }


  onUpdateClick(item: any){
    const modalRef = this.modalService.open(UpdateModalComponent);
    modalRef.componentInstance.getFormConfig = this.tableConfig.formConfig;
    modalRef.componentInstance.item = item;
    modalRef.componentInstance.tableConfigGetUpdateData = this.tableConfig.serviceData;
    modalRef.componentInstance.headingTitle = this.tableConfig.headingTitle;
      // Subscribe to updatedData EventEmitter from UpdateModalComponent
      modalRef.componentInstance.updatedData.subscribe((itemUpdated: any) => {
        if (itemUpdated) {
          this.updateData(itemUpdated); // Call updateData function with updatedItem
          
        }
      });
    
   
   
  }

  updateData(updatedItem: any) {
    for (let i = 0; i < this.tableConfig.tableData.length; i++) {
      if (this.tableConfig.tableData[i].id === updatedItem.id) {
        this.tableConfig.tableData[i] = updatedItem;
        break;
      }
    }
  }



  onDeleteClick(item: any) {
  
      const modalRef = this.modalService.open(DeleteModalComponent);
      // Pass data to the modal if needed
      console.log('Delete clicked ', item);
      modalRef.componentInstance.item = item;
      modalRef.componentInstance.tableConfigGetServiceData = this.tableConfig.serviceData;
   
  }

  sortTable(columnKey: string) {
    if (this.sortColumn === columnKey) {
      this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
      console.log(this.sortDirection, 'kjaksljjka')
    } else {
      this.sortColumn = columnKey;
      this.sortDirection = 'asc';
      console.log(this.sortDirection, 'bccc')
    }

    this.tableConfig.tableData.sort((a: any, b: any) => {
      const valueA = a[columnKey];
      console.log(valueA, 'value a')
      const valueB = b[columnKey];

      let comparison = 0;
      if (valueA > valueB) {
        comparison = 1;
        console.log(comparison, 'comparison1')
      } else if (valueA < valueB) {
        comparison = -1;
        console.log(comparison, 'comparison -1')
      }

      return this.sortDirection === 'asc' ? comparison : -comparison;
    });
  }

  getSortClass(columnKey: string) {
    if (this.sortColumn === columnKey) {
      return this.sortDirection === 'asc' ? 'sort-asc' : 'sort-desc';
    }
    return '';
  }


 
  
 

  
  

}
