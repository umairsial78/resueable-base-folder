import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-sidebar',
  templateUrl: './dashboard-sidebar.component.html',
  styleUrl: './dashboard-sidebar.component.scss'
})
export class DashboardSidebarComponent {

  
  constructor(private router: Router ){}
  

  logOutDashboard(){
    localStorage.removeItem('user');
    this.router.navigate(['/login']); 
    
    
  }

}
