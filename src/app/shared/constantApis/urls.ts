

export const urls = {

    stagingUrl: 'https://api.quotation.io-devs.us/api/',



    // Auth
    login: 'auth/login',
    getUsersList: 'administration/users/listing',
    deleteUser: 'administration/users/delete',
    updateUser: 'administration/users/update',
    roleList: 'administration/roles/listing',
    
    // customer
    customerList: 'phonebook/customers/listing',
    customerUpdate: 'phonebook/customers/update',
    customerDelete: 'phonebook/customers/delete'

}