import { Inject, Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateFn, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../../auth/login.service';


@Injectable({
  providedIn: 'root'
})


export class LoginGuard implements CanActivate{
  constructor(private userlogin: LoginService, private router: Router){}
  canActivate(
    route: ActivatedRouteSnapshot, 
    state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
      if (localStorage.getItem('user')) {
        return true;
      } else {
        this.router.navigate(['/login']);
        return false;
      }
      
     
  }
  

 

}
