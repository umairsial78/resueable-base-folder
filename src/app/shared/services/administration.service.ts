import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../Environment/environment';
import { urls } from '../constantApis/urls';
import { login } from '../../auth/data-types';

@Injectable({
  providedIn: 'root'
})
export class AdministrationService {

  constructor(private http: HttpClient) { }

  getUserList(data: {}): Observable<any> {
    console.log('showing user listing data:', data);
    return this.http.post<any>(`${environment.apiUrl + urls.getUsersList}`, data);
    
  }
  refreshUserList(data: {}): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl + urls.getUsersList}`, data);
    
  }
  deleteUserService(data: {}): Observable<any> {
    console.log('showing user delete data:', data);
    return this.http.post<any>(`${environment.apiUrl + urls.deleteUser}`, data);
    
  }

  updateUser(data: {}): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl + urls.updateUser}`, data);
    
  }

  roleListing(data: {}): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl + urls.roleList}`, data);
    
  }

  // customer

  getCustomerList(data: {}): Observable<any> {
    console.log('showing user listing data:', data);
    return this.http.post<any>(`${environment.apiUrl + urls.customerList}`, data);
    
  }

  updateCustomer(data: {}): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl + urls.customerUpdate}`, data);
    
  }
  deleteCustomer(data: {}): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl + urls.customerDelete}`, data);
    
  }


  // userService(data: {}, url: ''): Observable<any> {
  //   console.log('showing user listing data:', data, url);
  //   return this.http.post<any>(`${environment.apiUrl + url}`, data);
    
  // }
}
