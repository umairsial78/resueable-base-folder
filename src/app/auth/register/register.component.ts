import { Component } from '@angular/core';
import { SignUp } from '../data-types';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.scss'
})
export class RegisterComponent {

  constructor(private userSignup: LoginService){}

  signUp(data: SignUp): void{
    console.log(data, 'show data')
    // this.userSignup.userSignUp(data)
  }

}
