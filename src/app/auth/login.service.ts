import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { login } from './data-types';
import { Observable, catchError, tap, throwError } from 'rxjs';
import { environment } from '../../Environment/environment';
import { urls } from '../shared/constantApis/urls';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
  isLoginError= new EventEmitter<boolean>(false);
  

  constructor(private http: HttpClient) { }

  userLogin(data: login): Observable<any> {
    console.log('Sending login request with data:', data);
    return this.http.post<any>(`${environment.apiUrl + urls.login}`, data);
    
  }




}
