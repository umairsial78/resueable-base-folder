import { Component } from '@angular/core';
import { SignUp, login } from '../data-types';

import { ToastrService } from 'ngx-toastr';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent {
  isLoginShow = false;
  authError: string= "";
  hideEror= false;

  
  

  constructor(private loginService: LoginService, private router: Router, private toaster: ToastrService) { }

  onLogin(data: login) {
    
    this.loginService.userLogin(data).subscribe(
      (response) => {
        console.log('Login successful', response);
        localStorage.setItem('user', JSON.stringify(response.data));
        this.toaster.success('Login successful!')
        this.router.navigate(['/home']);

      },
      error => {
        console.error('Login failed', error);
      }
    );
  }
 

  Loginshow(){
    this.isLoginShow = true;
  }

  SignupShow(){
    this.isLoginShow = false;
  }

 



}
