import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from '../shared/components/header/header.component';
import { FooterComponent } from '../shared/components/footer/footer.component';
import { HomeRoutingModule } from './home-routing.module';
import { RouterModule } from '@angular/router';
import { AuthModule } from '../auth/auth.module';
import { DashboardSidebarComponent } from '../shared/components/dashboard-sidebar/dashboard-sidebar.component';
import { TableComponent } from '../shared/components/table/table.component';
import { UserComponent } from './user/user.component';
import { DeleteModalComponent } from '../shared/components/delete-modal/delete-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FieldsTypeComponent } from '../shared/components/fields-type/fields-type.component';
import { CheckboxComponent } from '../shared/components/checkbox/checkbox.component';
import { RadioButtonComponent } from '../shared/components/radio-button/radio-button.component';
import { SwitchToggleComponent } from '../shared/components/switch-toggle/switch-toggle.component';
import { UpdateModalComponent } from '../shared/components/update-modal/update-modal.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgSelectComponent } from '../shared/components/ng-select/ng-select.component';
import { CustomersComponent } from './customers/customers.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgChartsModule } from 'ng2-charts';
import { NgApexchartsModule } from 'ng-apexcharts';









@NgModule({
  declarations: [
    HomeComponent,
    FooterComponent,
    DashboardSidebarComponent,
    UserComponent,
    TableComponent,
    DeleteModalComponent,
    FieldsTypeComponent,
    CheckboxComponent,
    RadioButtonComponent,
    SwitchToggleComponent,
    UpdateModalComponent,
    NgSelectComponent,
    CustomersComponent,
    DashboardComponent
    
  ],
  imports: [
    CommonModule,
    HeaderComponent,
    HomeRoutingModule,
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule,
    RouterModule,
    AuthModule,
    NgApexchartsModule,
    NgChartsModule
   
 
  ]
})
export class HomeModule { }
