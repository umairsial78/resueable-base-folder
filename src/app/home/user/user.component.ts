import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { DeleteModalComponent } from '../../shared/components/delete-modal/delete-modal.component';
import { AdministrationService } from '../../shared/services/administration.service';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrl: './user.component.scss'
})
export class UserComponent implements OnInit {

userListigData= [];
roleListingData = [];
searchQuery: string = '';
headingTitle: string = 'User';

constructor(private toaster: ToastrService, private modalService: NgbModal, private userServices: AdministrationService) { }

tableConfig = {

 tableColumns: [
     {
         title: 'name', columnKey: 'name', type: 'input', isEditable: false, showColumn: true, sortColumn: false
     },
     {
         title: 'user Name', columnKey: 'username', type: 'input', isEditable: false, showColumn: true, sortColumn: false
     },
     {
         title: 'email', columnKey: 'email', type: 'email', isEditable: false, showColumn: true, sortColumn: false
     },
     {
         title: 'created_at', columnKey: 'created_at', type: 'date', isEditable: false, showColumn: true, sortColumn: false
     },
     {
         title: 'email_verified_at', columnKey: 'email_verified_at', type: 'date', isEditable: false, showColumn: true, sortColumn: false
     },
     {
         title: 'employees', columnKey: 'employees', type: 'object', isEditable: false, showColumn: false, sortColumn: false
     },
     {
         title: 'last_used_at', columnKey: 'last_used_at', type: 'date', isEditable: false, showColumn: true, sortColumn: false
     },
     {
         title: 'profile_picture', columnKey: 'profile_picture', type: 'image', isEditable: false, showColumn: false, sortColumn: false
     },
     {
         title: 'role', columnKey: 'role', type: 'object', isEditable: false, showColumn: true, sortColumn: false
     },
    //  {
    //   title: 'role_id', columnKey: 'role_id', type: 'id', isEditable: false, showColumn: false, sortColumn: false
    // },
    
     {
         title: 'updated_at', columnKey: 'updated_at', type: 'date', isEditable: false, showColumn: true, sortColumn: false
     },
     {
      title: 'status', columnKey: 'status', type: 'toggle', isEditable: false, showColumn: true, sortColumn: false
     },
     {
      title: 'Actions', columnKey: 'Actions', type: 'button', isEditable: false, showColumn: true, sortColumn: false
     },
 ],
 tableActions: ['edit', 'delete', 'view'],
 headingTitle: this.headingTitle,
 tableData: this.userListigData,
 serviceData: {url: 'administration/users', service: this.userServices, apiFunctions: {delete: 'deleteUserService', refresh: 'refreshUserList', view: 'viewUser', update: 'updateUser'}},

 formConfig: [
      {
        label: 'Name',
        name: 'name',
        type: 'text',
        validations: [
          { name: 'required', validator: Validators.required, message: 'Name is required' },
        ],
      },
      {
        label: 'Email',
        name: 'email',
        type: 'email',
        validations: [
          { name: 'required', validator: Validators.required, message: 'Email is required' },
          { name: 'pattern', validator: Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"), message: 'Invalid email format' },
          
        ],
    },
    {
      label: 'User Name',
      name: 'username',
      type: 'text',
      validations: [
        { name: 'required', validator: Validators.required, message: 'User name is required' },
        // { name: 'min', validator: Validators.min(18), message: 'Minimum age is 18' },
      ],
    },
    {
      label: 'Role',
      customKey: 'keyValue',
      name: 'role_id',
      type: 'dropdown',
      data: this.roleListingData,
      validations: [
        { name: 'required', validator: Validators.required, message: 'role id  is required' },
        
      ],
    
    },
    {
      name: 'id',
      
    },
    
  ],
 
};



roleListingDataFc() {
  this.userServices.roleListing({}).subscribe((result) => {
    if (result) {
      this.roleListingData = result.data;
      const roleConfig = this.tableConfig.formConfig.find(field => field.customKey === 'keyValue');
      if (roleConfig) {
        roleConfig.data = this.roleListingData;
       
      } 
    }
  });
}
 
  checkStatus() {
    this.userServices.getUserList({}).subscribe((res) => {
      if (res && res.data && res.data.data) {
        this.userListigData = res.data.data;
        // console.log(this.userListigData, 'showing data');
        this.tableConfig.tableData = this.userListigData;
       
        

      
      }
    });

  }

  ngOnInit(): void {
      this.checkStatus();
      this.roleListingDataFc();
  }




  

  // Your other code...

  onSearch(query: string) {
    this.searchQuery = query;
    this.tableConfig.tableData = this.userListigData.filter(item =>
      Object.values(item).some(val =>
        String(val).toLowerCase().includes(this.searchQuery.toLowerCase())
      )
    );
  }






  onEditItem(item: any) {
    // this.toaster.success('Edit clicked ');
    console.log('Edit clicked ', item)
    // Handle edit action here...
  }

  // handleDataUpdate() {
  //   // Reload user data from the server
  //   this.checkStatus();
  // }
 
 




 
 

}


