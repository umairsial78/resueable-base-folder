import { Component } from '@angular/core';
import { ApexAxisChartSeries, ApexChart, ApexTitleSubtitle } from 'ng-apexcharts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  public series: ApexAxisChartSeries = [
    {
      name: 'Series A',
      data: [30, 40, 45, 50, 49, 60, 70, 91],
      color: '#48b649'
    },
    {
      name: 'Series B',
      data: [40, 50, 55, 60, 61, 70, 80, 91],
      color: '#e67e22'
    },
    {
      name: 'Series C',
      data: [10, 28, 33,49, 86, 70, 50, 100],
      color: '#86efbd'
    }
  ];

  public chart: ApexChart = {
    type: 'line',
    height: 650
    
  };

  public title: ApexTitleSubtitle = {
    text: 'Basic Bar Chart'
  };



  public seriesTwo: ApexAxisChartSeries = [
    {
      name: 'Series A',
      data: [30, 40, 45, 50, 49, 60, 70, 91],
      color: '#48b649'
    },
    {
      name: 'Series B',
      data: [40, 50, 55, 60, 61, 70, 80, 91],
      color: '#e67e22'
    },
    {
      name: 'Series C',
      data: [10, 28, 33,49, 86, 70, 50, 100],
      color: '#86efbd'
    }
  ];

  public chartTwo: ApexChart = {
    type: 'bar',
    height: 650
    
  };

  public titleTwo: ApexTitleSubtitle = {
    text: 'Basic Bar Chart'
  };











}
