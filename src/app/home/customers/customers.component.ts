import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { AdministrationService } from '../../shared/services/administration.service';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrl: './customers.component.scss'
})
export class CustomersComponent {

    customerListigData= [];
    roleListingData = [];
    searchQuery: string = '';
    headingTitle: string = 'Customer';

  
    
    constructor(private toaster: ToastrService, private modalService: NgbModal, private userServices: AdministrationService) {
      
     }
    
    tableConfig = {
    
     tableColumns: [
         {
             title: 'name', columnKey: 'name', type: 'input', isEditable: false, showColumn: true, sortColumn: true
         },
         {
             title: 'sage_customer_name', columnKey: 'sage_customer_name', type: 'input', isEditable: false, showColumn: false, sortColumn: false
         },
         {
             title: 'email', columnKey: 'email', type: 'email', isEditable: false, showColumn: true, sortColumn: false
         },
         {
             title: 'phone_no', columnKey: 'phone_no', type: 'array', isEditable: false, showColumn: true, sortColumn: false
         },
         {
             title: 'address', columnKey: 'address', type: 'input', isEditable: false, showColumn: false, sortColumn: false
         },
         {
          title: 'sage_customer_id', columnKey: 'sage_customer_id', type: 'number', isEditable: false, showColumn: true, sortColumn: false
         },
         {
          title: 'created_at', columnKey: 'created_at', type: 'date', isEditable: false, showColumn: true, sortColumn: false
         },
         {
          title: 'updated_at', columnKey: 'updated_at', type: 'date', isEditable: false, showColumn: true, sortColumn: false
        },
      
         {
             title: 'role', columnKey: 'role', type: 'object', isEditable: false, showColumn: false, sortColumn: false
         },
        
         {
          title: 'blacklist', columnKey: 'blacklist', type: 'toggle', isEditable: false, showColumn: true, sortColumn: false
         },
         {
          title: 'Actions', columnKey: 'Actions', type: 'button', isEditable: false, showColumn: true, sortColumn: false
         },
     ],
     tableActions: ['edit', 'delete', 'view'],
     headingTitle: this.headingTitle,
     tableData: this.customerListigData,
     serviceData: {url: 'administration/users', service: this.userServices, apiFunctions: {delete: 'deleteCustomer', refresh: 'refreshUserList', view: 'viewUser', update: 'updateCustomer'}},
    
     formConfig: [
          {
            label: 'Name',
            name: 'name',
            type: 'text',
            validations: [
              { name: 'required', validator: Validators.required, message: 'Name is required' },
            ],
          },
          {
            label: 'Email',
            name: 'email',
            type: 'email',
            validations: [
              { name: 'required', validator: Validators.required, message: 'Email is required' },
              { name: 'pattern', validator: Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$"), message: 'Invalid email format' },
              
            ],
        },
        {
          label: 'Phone no',
          name: 'phone_no',
          type: 'array',
          validations: [
            { name: 'required', validator: Validators.required, message: 'Number is required' },
            // { name: 'min', validator: Validators.min(18), message: 'Minimum age is 18' },
          ],
        },
     
        {
          label: 'blacklist',
          name: 'blacklist',
          type: 'toggle',
          validations: [
            { name: 'required', validator: Validators.required, message: 'toggle is required' },
           
          ],
        },
     

        {
          name: 'id',
          
        },
        
      ],
     
    };
    
    
    
   
     
      checkStatus() {
        this.userServices.getCustomerList({}).subscribe((res) => {
          if (res && res.data && res.data.data) {
            this.customerListigData = res.data.data;
            // console.log(this.userListigData, 'showing data');
            this.tableConfig.tableData = this.customerListigData;
           
            // this.tableConfig.formConfig = this.tableConfig.tableData;
            // console.log(this.tableConfig.tableData, 'asasa')
            // this.tableConfig.formConfig.toggleValue = this.customerListigData;
    
          
          }
        });
    
      }
    
      ngOnInit(): void {
          this.checkStatus();
         
      }
    
    
    
      // Your other code...
    
      onSearch(query: string) {
        this.searchQuery = query;
        this.tableConfig.tableData = this.customerListigData.filter(item =>
          Object.values(item).some(val =>
            String(val).toLowerCase().includes(this.searchQuery.toLowerCase())
          )
        );
      }

     
  
    
    }
    


