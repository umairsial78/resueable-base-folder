import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TableComponent } from '../shared/components/table/table.component';
import { UserComponent } from './user/user.component';
import { CustomersComponent } from './customers/customers.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    // redirectTo: 'table',
    // pathMatch:  'full',
    component: HomeComponent,
   
    children: [
      {
        path: 'table',
        component: TableComponent,
      },
      {
        path: 'user',
        component: UserComponent,
      },
      {
        path: 'customers',
        component: CustomersComponent,
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
      },
    ],
  },
  
  // {
  //   path: 'table',
  //   component: TableComponent,
  // },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
