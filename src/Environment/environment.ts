import { urls } from "../app/shared/constantApis/urls";


export const environment = {
    production: false,
    apiUrl: urls.stagingUrl,
};